package com.eztravel.checkout;

import java.util.List;

public class CheckoutService {

	public int checkout(String fruits, Integer unit) {
		if ("Apple".equals(fruits)) {
			return 20 * unit;
		}
		else if ("Banana".equals(fruits)) {
			return 30 * unit;
		}
		else if ("Orange".equals(fruits)) {
			return 50 * unit;
		}
		else if ("Cherry".equals(fruits)) {
			return 100 * unit;
		}

		return 0;
	}

	public int checkout(List<Fruit> fruits) {

		int total = 0;
		for (Fruit fruit : fruits) {
			if ("Banana".equals(fruit.getName())) {
				int bananas = fruit.getUnit() / 3;
				int otherbananas = fruit.getUnit() % 3;
				total = total + checkout("Banana", 2) * bananas;
				if (otherbananas != 0) {
					total = total + checkout("Banana", 1) * otherbananas;
				}
			}else if(true == fruit.isOnsale && "Apple".equals(fruit.getName())){				
					int groups = fruit.getUnit() / 2;
					int remainder = fruit.getUnit() % 2;
					return (int) ((40 * groups) * 0.75) + remainder * 20;	
			}else {				
				total += checkout(fruit.getName(), fruit.getUnit());
			}
		}
		return total;
	}

}
