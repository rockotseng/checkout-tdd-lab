package com.eztravel.checkout;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Apple $20
 * Banana $30
 * Orange $50
 * Cherry $100 
 */
public class CheckoutServiceTest {

	private CheckoutService checkoutService;
	
	@Before
	public void setup() {
		checkoutService = new CheckoutService();
	}
	
	@Test
	public void testCheckoutBuyOneApple() {
		assertEquals(20, checkoutService.checkout("Apple", 1));
	}
	
	@Test
	public void testCheckoutBuyApples() {
		assertEquals(100, checkoutService.checkout("Apple", 5));
	}
	
	/**
	 * Fruit Box includes
	 * Apple x 1 
	 * Banana x 1
	 * Orange x 2
	 * Cherry x 1
	 */
	@Test
	public void testCheckoutBuyFruitBox() {
		List<Fruit> fruits = new ArrayList<Fruit>();
		Fruit apple = new Fruit();
		apple.setName("Apple");
		apple.setUnit(1);
		fruits.add(apple);
		Fruit banana = new Fruit();
		banana.setName("Banana");
		banana.setUnit(1);
		fruits.add(banana);
		Fruit orange = new Fruit();
		orange.setName("Orange");
		orange.setUnit(2);
		fruits.add(orange);
		Fruit cherry = new Fruit();
		cherry.setName("Cherry");
		cherry.setUnit(1);
		fruits.add(cherry);
		
		assertEquals(250, checkoutService.checkout(fruits));
	}
	
	/**
	 * 買3根香蕉 第3根免費
	 */
	@Test
	public void testCheckoutBananaOnSale() {
		List<Fruit> bananas = new ArrayList<Fruit>();
		Fruit apple = new Fruit();
		apple.setName("Banana");
		apple.setUnit(3);
		bananas.add(apple);
		assertEquals(60, checkoutService.checkout(bananas));

		apple.setUnit(5);
		assertEquals(120, checkoutService.checkout(bananas));
	}
	
	/**
	 * 買2顆apple 第2顆半價 
	 */
	@Test
	public void testCheckoutAppleOnSale() {
		List<Fruit> apples = new ArrayList<Fruit>();
		Fruit apple = new Fruit();
		apple.setName("Apple");
		apple.setUnit(2);
		apple.setOnsale(true);
		apples.add(apple);
		
		assertEquals(30, checkoutService.checkout(apples));
		
		apple.setUnit(3);
		assertEquals(50, checkoutService.checkout(apples));
		
		apple.setUnit(6);
		assertEquals(90, checkoutService.checkout(apples));
	}
	

}
